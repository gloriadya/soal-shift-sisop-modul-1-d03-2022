#!/bin/bash

### membuat folder bernama forensic_log_website_daffainfo_log
folderlog="forensic_log_website_daffainfo_log/"
log="log_website_daffainfo.log"
if [ ! -d $folderlog ]; then
    mkdir $folderlog
else
    rm -rf $folderlog
    mkdir $folderlog
fi

### rata-rata request per jam 
cat $log | awk ' END {print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "requests per jam"}'>$folderlog/ratarata.txt

### IP paling banyak melakukan request
cat $log | awk -F\" '{print $2}' $log | sort | uniq -c | sort -rn | head -n1 | awk '{print "IP yang paling banyak mengakses server adalah:", $2, "sebanyak", $1, "requests"} '>$folderlog/result.txt

### banyak request yang menggunakan user-agent curl
printf "\n\n">>result.txt
cat $log | awk '/curl/ { n++ } END{ print "Ada", n ,"request yang menggunakan curl sebagai user-agent" }'>>$folderlog/result.txt

### daftar IP yang mengakses pada jam 2 pagi
cat $log| awk -F':|"' '{if($6 == 2)print $2}' $log | sort | uniq >> $folderlog/result.txt
