cekpass(){
    local location=/home/naufalfabian
	local lengthPassword=${#password}

	if [[ $lengthPassword -lt 8 ]]
    	then
        	echo "Password must be more than 8 characters"

	elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
	then
       		echo "Password must contain upper, lower and number!"
    	else
		login
	fi
}

login(){
    local location=/home/naufalfabian

	if [[ ! -f "$location/user.txt" ]]
	then
		echo "user not registered yet"
	else
		if grep -q -w "$username" "$location/user.txt"
		then
			if grep -q -w "$username $password" "$location/user.txt"
			then
				echo $calendar $time "LOGIN:INFO User $username logged in" >> $location/log.txt
				echo "Login done"

				printf "Enter command option [dl or att]: "
				read command total
				if [[ $command == att ]]
				then
					att
				elif [[ $command == dl ]] && [[  -n "$total" ]]
				then
					dl_pic
				else
					echo "Command not found, retry again"
				fi

			else
				fail="Failed login attemp on user $username"
				echo $fail

				echo $calendar $time "LOGIN:ERROR $fail" >> $location/log.txt
			fi
		else
			echo "User not found, please register first!"
		fi
	fi
}

dl_pic(){
	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		count=0
		start_dl
	else
		funzip
	fi

}

funzip(){
	unzip -P $password $folder.zip
	rm $folder.zip

	count=$(find $folder -type f | wc -l)
	start_dl
}

start_dl(){
	for(( i=$count+1; i<=$total+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip -P $password -r $folder.zip $folder/
	rm -rf $folder
}

att(){
    local location=/home/naufalfabian
	if [[ ! -f "$location/log.txt" ]]
	then
		echo "Log not found"
	else
		awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count-1)}' $location/log.txt
	fi
}


# main
calendar=$(date +%D)
time=$(date +%T)

printf "Enter your username: "
read username

printf "Enter your password: "
read -s password

# deff dir
folder=$(date +%Y-%m-%d)_$username
location=/home/naufalfabian/


if [[ $(id -u) -ne 0 ]]
then
	echo "Please run as root"
	exit 1
else
	cekpass
fi
