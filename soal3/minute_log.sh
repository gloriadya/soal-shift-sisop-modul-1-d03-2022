#!/bin/bash

dates=$(date '+%Y%m%d%H%M%S')

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/selfira/log/metrics_$dates.log

free -m > freem.log
awk '{ORS=""}FNR==2{print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' freem.log >> /home/selfira/log/metrics_$dates.log
awk '{ORS=""}FNR==3{print $2 "," $3 "," $4 ","}' freem.log >> /home/selfira/log/metrics_$dates.log

du -sh /home/selfira > dush.log
awk '{ORS=""}{print $2 "," $1}' dush.log >> /home/selfira/log/metrics_$dates.log
