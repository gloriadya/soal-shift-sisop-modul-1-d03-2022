#!/bin/bash

dateperhour=$(date "+%Y%m%d%H")

file="metrics_agg_$dateperhour.log"

find /$HOME/log -name "metrics_20*" -mmin -60 -exec cat {} + > /$HOME/log/hour.log

sed -n '1p' /$HOME/log/hour.log > /$HOME/log/$file

sed -i '/^mem/d' /$HOME/log/hour.log

echo -n "minimum," >> /$HOME/log/$file

awk -F "\"*,\"*" '{ORS=","}min=="" || $1 < min {min=$1} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $2 < min {min=$2} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $3 < min {min=$3} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $4 < min {min=$4} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $5 < min {min=$5} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $6 < min {min=$6} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $7 < min {min=$7} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $8 < min {min=$8} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}min=="" || $9 < min {min=$9} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file
echo -n "$HOME," >> /$HOME/log/$file
awk -F "\"*,\"*" 'min=="" || $11 < min {min=$11} END {print min}' /$HOME/log/hour.log >> /$HOME/log/$file

echo -n "maximum," >> /$HOME/log/$file

awk -F "\"*,\"*" '{ORS=","}max=="" || $1 > max {max=$1} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $2 > max {max=$2} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $3 > max {max=$3} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $4 > max {max=$4} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $5 > max {max=$5} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $6 > max {max=$6} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $7 > max {max=$7} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $8 > max {max=$8} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}max=="" || $9 > max {max=$9} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file
echo -n "$HOME," >> /$HOME/log/$file
awk -F "\"*,\"*" 'max=="" || $11 > max {max=$11} END {print max}' /$HOME/log/hour.log >> /$HOME/log/$file

echo -n "average," >> /$HOME/log/$file

awk -F "\"*,\"*" '{ORS=","}{ total+=$1 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$2 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$3 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$4 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$5 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$6 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$7 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$8 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
awk -F "\"*,\"*" '{ORS=","}{ total+=$9 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file
echo -n "$HOME," >> /$HOME/log/$file
awk -F "\"*,\"*" '{ total+=$11 } END { print total/NR }' /$HOME/log/hour.log >> /$HOME/log/$file

chmod 700 /$HOME/log/$file
