# soal-shift-sisop-modul-1-D03-2022

Kelompok D 03 \
Anggota Kelompok 
|Nama                   |     NRP|
|-----------------------|----------------------|
|Naufal Fabian Wibowo    |    05111940000223|
|Gloria Dyah Pramesti    |    5025201033|
|Selfira Ayu Sheehan     |    5025201174|

Soal 1

Penjelasan
* 1.1 membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. membuat sistem login yang dibuat di script main.sh
* 1.2 input password pada login dan register harus tertutup/hidden 
* 1.2.a password Minimal 8 karakter
* 1.2.b password memiliki minimal 1 huruf kapital dan 1 huruf kecil
* 1.2.c password alphanumeric
* 1.2.d password tidak boleh sama dengan username
* 1.3 Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE
* 1.3.a register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
* 1.3.b register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
* 1.3.c user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
* 1.3.d user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
* 1.4 Setelah login, user dapat mengetikkan 2 command
* 1.4.a dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
* 1.4.b att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

Hasil

* input password hidden

![Screenshot_2022-03-06_180051](/uploads/e792cecf0e619e7fc02f329732165b48/Screenshot_2022-03-06_180051.png)

* register berhasil

![Screenshot_2022-03-06_173510](/uploads/16f42304bed3f49c598d0b4058353284/Screenshot_2022-03-06_173510.png)

* isi user.txt

![Screenshot_2022-03-06_173533](/uploads/6224c67bbb54ac74f2cb8cab0df50734/Screenshot_2022-03-06_173533.png)

* register password tidak lebih dari 8 karakter

![Screenshot_2022-03-06_180010](/uploads/86045aedb9c1b3350b745853c95c5324/Screenshot_2022-03-06_180010.png)

* register password tidak ada uppercase

![Screenshot_2022-03-06_180122](/uploads/d3fbe5e6635c4763596bf5c9332e5f9b/Screenshot_2022-03-06_180122.png)

* register password tidak ada lowercase

![Screenshot_2022-03-06_180203](/uploads/84baf23f213afc150605fac388daa3c0/Screenshot_2022-03-06_180203.png)

* register password tidak ada angka

![Screenshot_2022-03-06_180339](/uploads/3983f7fed32b31b7170aaa728fd8551e/Screenshot_2022-03-06_180339.png)

* register password sama dengan username

![Screenshot_2022-03-06_180420](/uploads/2f347a729b3d6989e87ad9fee349f70b/Screenshot_2022-03-06_180420.png)

* register username sudah terpakai

![Screenshot_2022-03-06_180548](/uploads/5c6415871c00c8b396d9040f6aea40da/Screenshot_2022-03-06_180548.png)

* login password salah 

![Screenshot_2022-03-06_180741](/uploads/440f2bb43a6ad9512362abe51181b17f/Screenshot_2022-03-06_180741.png)

* login berhasil

![Screenshot_2022-03-06_180829](/uploads/62995d8871402f62fce0a59d94010798/Screenshot_2022-03-06_180829.png)

* command att

![Screenshot_2022-03-06_180859](/uploads/a7426300c34eb7eca5b1521a44737eee/Screenshot_2022-03-06_180859.png)

* command dl N

![Screenshot_2022-03-06_181052](/uploads/14ce51453b60c653ccc4ec45605cdca9/Screenshot_2022-03-06_181052.png)

* file direktori

![Screenshot_2022-03-06_181312](/uploads/204b061c40422daac11a65048c0fbef6/Screenshot_2022-03-06_181312.png)

* isi log.txt

![Screenshot_2022-03-06_184154](/uploads/bb8181ba593ca41b2fb07015c174653c/Screenshot_2022-03-06_184154.png)



Soal 2

Penjelasan
* 2a. Membuat folder bernama forensic_log_website_daffainfo_log dengan mkdir $folderlog
* 2b. Mencari rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian dimasukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt
* 2c. Menampilkan IP yang paling banyak melakukan request ke server dan menampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Hasil disimpan kedalam file baru bernama result.txt
* 2d. Mencari berapa banyak requests yang menggunakan user-agent curl
* 2e. Mencari tahu daftar IP yang mengakses website pada jam 2 pagi

Hasil
![Screenshot_from_2022-03-06_16-00-45](/uploads/ed7de6cfd04db652b0e24a0deaaea5aa/Screenshot_from_2022-03-06_16-00-45.png)

Soal 3

Penjelasan
* 3a. Membuat file log dengan format metrics_YmdHMS.log yang memuat isi free -m dan du -sh menggunakan awk, dimana isi free -m dan du -sh dimasukkan ke file terpisah lalu dijalankan awk untuk mengambil isi free -m dan du -sh
``` bash 
#!/bin/bash

dates=$(date '+%Y%m%d%H%M%S')

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/selfira/log/metrics_$dates.log

free -m > freem.log
awk '{ORS=""}FNR==2{print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' freem.log >> /home/selfira/log/metrics_$dates.log
awk '{ORS=""}FNR==3{print $2 "," $3 "," $4 ","}' freem.log >> /home/selfira/log/metrics_$dates.log

du -sh /home/selfira > dush.log
awk '{ORS=""}{print $2 "," $1}' dush.log >> /home/selfira/log/metrics_$dates.log
```
* 3b. Menjalankan minute_log.sh per menit dengan menggunakan crontab
``` bash
* * * * * /home/selfira/minute_log.sh
```
* 3c. Membuat file log yang memuat agregasi (minimum, maximum, dan rata-rata) dari isi free -m dan du -sh per jamnya, dengan cara menyimpan isi setiap jam lalu dilakukan perbandingan pada tiap rownya
* 3d. Memastikan hanya user pemilik file yang bisa membaca semua file log dengan menggunakan chmod

Hasil
![soal3](/uploads/b0229733742104a58331599dae08070b/soal3.jpg)
![soal3_1](/uploads/de92776fd44395f3474efedbad10ea1c/soal3_1.jpg)
![soal3_2](/uploads/f227e23bde895a55fa9de26e8092d6e3/soal3_2.jpg)

Kendala
* Belum familiar dengan tools gitlab ini sehingga menimbulkan kesusahan dalam menambahkan file hasil pengerjaan
* Terdapat materi yang belum dipahami sehingga harus banyak riset
